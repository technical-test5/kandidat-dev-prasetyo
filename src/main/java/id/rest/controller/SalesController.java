package id.rest.controller;

import id.rest.model.dto.*;
import id.rest.service.SalesService;
import id.rest.util.ApiResponseUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.api.annotations.ParameterObject;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/sales")
public class SalesController {

    @Autowired
    private SalesService service;


    @Tag(name = "Sales Rest")
    @Operation(summary = "Get products (paginate)", description = "Get paginate product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found movies",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProductDto.class)) }),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content),
    })
    @PageableAsQueryParam
    @GetMapping("/products")
    public ResponseEntity<Page<ProductDto>> products(
//            @ParameterObject SalesSearchDto filter,
            @ParameterObject @PageableDefault(sort = {"name"}, size = 10, direction = Sort.Direction.ASC) final Pageable pageable) {
        try {
            Page<ProductDto> special = service.getProducts(pageable);
            return new ResponseEntity(new ApiResponseUtil<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @Tag(name = "Sales Rest")
    @Operation(summary = "Get customer (paginate)", description = "Get paginate customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found movies",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CustomerDto.class)) }),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content),
    })
    @PageableAsQueryParam
    @GetMapping("/customers")
    public ResponseEntity<Page<CustomerDto>> customers(
//            @ParameterObject SalesSearchDto filter,
            @ParameterObject @PageableDefault(sort = {"name"}, size = 10, direction = Sort.Direction.ASC) final Pageable pageable) {
        try {
            Page<CustomerDto> special = service.getCustomer(pageable);
            return new ResponseEntity(new ApiResponseUtil<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @Tag(name = "Sales Rest")
    @Operation(summary = "Get Saless (paginate)", description = "Get paginate invoice")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found movies",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = SalesInvoiceDto.class)) }),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content),
            })
    @PageableAsQueryParam
    @GetMapping()
    public ResponseEntity<Page<SalesInvoiceDto>> list(
//            @ParameterObject SalesSearchDto filter,
            @ParameterObject @PageableDefault(sort = {"id"}, size = 10, direction = Sort.Direction.DESC) final Pageable pageable) {
        try {
            Page<SalesInvoiceDto> special = service.getSalesInvoice(pageable);
            return new ResponseEntity(new ApiResponseUtil<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @Tag(name = "Sales Rest")
    @Operation(summary = "Get sales invoice detail", description = "Get sales invoice detail by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully update movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = SalesInvoiceDetailDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid parameter",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content),
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        try {
            SalesInvoiceDetailDto special = service.getSalesInvoiceDetail(id);
            return new ResponseEntity(new ApiResponseUtil<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

//    @Tag(name = "Sales Rest")
//    @Operation(summary = "update movie", description = "Update movie by id")
//    @ApiResponses(value = {
//            @ApiResponse(responseCode = "200", description = "Successfully update movie",
//                    content = { @Content(mediaType = "application/json",
//                            schema = @Schema(implementation = SalesDto.class)) }),
//            @ApiResponse(responseCode = "400", description = "Invalid parameter",
//                    content = @Content),
//            @ApiResponse(responseCode = "500", description = "Internal Server Error",
//                    content = @Content),
//    })
//    @PutMapping("/{id}")
//    public ResponseEntity<?> put(@PathVariable(required = true) Long id, @Valid @RequestBody SalesPostDto input) {
//        try {
//            SalesDetailDto result = service.getById(id);
//            if (result == null) {
//                return ResponseEntity.notFound().build();
//            }
//            SalesDto special = service.update(id, input);
//            return new ResponseEntity(new ApiResponseUtil<>("200", "success", special), HttpStatus.OK);
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
//        }
//    }

    @Tag(name = "Sales Rest")
    @Operation(summary = "Submit sales")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Transaksi berhasil",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid parameter",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content),
    })
    @PostMapping("")
    public ResponseEntity<?> addSales(@Valid @RequestBody SalesPostDto input) {
        try {
            String special = service.submitSales(input);
            return new ResponseEntity(new ApiResponseUtil("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }


}

