package id.rest.model.mapper;

import id.rest.config.MapperInjectConfig;
import id.rest.model.dto.SalesInvoiceDetailDto;
import id.rest.model.dto.SalesInvoiceDto;
import id.rest.model.entity.SalesInvoice;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class, uses = {SalesInvoiceLineMapper.class})
public interface SalesInvoiceMapper {

    @Mappings({
            @Mapping(target = "customerId", source = "customer.id"),
            @Mapping(target = "customerName", source = "customer.name"),
    })
    public SalesInvoiceDto toDto(SalesInvoice entity);

    @Mappings({
            @Mapping(target = "customerId", source = "customer.id"),
            @Mapping(target = "customerName", source = "customer.name"),
            @Mapping(target = "details", source = "salesInvoiceLines"),
    })
    public SalesInvoiceDetailDto toDetailDto(SalesInvoice entity);

    @Mappings({
    })
    public SalesInvoice toEntity(SalesInvoiceDto dto);

}
