package id.rest.model.mapper;

import id.rest.config.MapperInjectConfig;
import id.rest.model.dto.ProductDto;
import id.rest.model.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class)
public interface ProductMapper {

    @Mappings({
    })
    public ProductDto toDto(Product entity);

    @Mappings({
    })
    public Product toEntity(ProductDto dto);

}
