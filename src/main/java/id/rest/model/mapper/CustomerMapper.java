package id.rest.model.mapper;

import id.rest.config.MapperInjectConfig;
import id.rest.model.dto.CustomerDto;
import id.rest.model.entity.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class)
public interface CustomerMapper {

    @Mappings({
    })
    public CustomerDto toDto(Customer entity);

    @Mappings({
    })
    public Customer toEntity(CustomerDto dto);

}
