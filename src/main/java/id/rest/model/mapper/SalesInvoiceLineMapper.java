package id.rest.model.mapper;

import id.rest.config.MapperInjectConfig;
import id.rest.model.dto.SalesInvoiceLineDto;
import id.rest.model.entity.SalesInvoiceLine;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class, uses = {ProductMapper.class})
public interface SalesInvoiceLineMapper {

    @Mappings({
            @Mapping(target = "product", source = "product"),
    })
    public SalesInvoiceLineDto toDto(SalesInvoiceLine entity);

    @Mappings({
    })
    public SalesInvoiceLine toEntity(SalesInvoiceLineDto dto);

}
