/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rest.model.specification;

import id.rest.model.dto.ProductDto;
import id.rest.model.entity.Product;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author mangprang
 */

public class SampleSpecification extends QuerySpecification<Product> {

    private Specification<Product> byName(String param) {
        return attributeContains("name", param);
    }

//    private Specification<Product> byDirector(String param) {
//        return relationAttributeContains("director.name", param);
//    }

    public Specification<Product> byEntitySearch(ProductDto filter) {
        if (filter == null) {
            return null;
        }
        return byName(filter.getName())
                ;
    }
}
