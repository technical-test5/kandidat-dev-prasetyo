package id.rest.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import id.rest.model.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SalesInvoiceDto implements Serializable {

  private Long id;
  private String invoiceNumber;
  private LocalDate invoiceDate;
  private Double amount;
  private Double totalDiscountAmount;
  private Double totalAmount;

  private Long customerId;
  private String customerName;

}
