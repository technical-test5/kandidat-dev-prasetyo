/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rest.model.repository;

import id.rest.model.entity.SalesInvoiceLine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author mangprang
 */
public interface SalesInvoiceLineRepository extends JpaRepository<SalesInvoiceLine, Long> {

    public Page<SalesInvoiceLine> findAll(Specification<SalesInvoiceLine> spec, Pageable pageable);
    public List<SalesInvoiceLine> findAll(Specification<SalesInvoiceLine> spec);

}
