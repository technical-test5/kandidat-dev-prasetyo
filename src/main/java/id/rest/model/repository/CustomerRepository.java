/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rest.model.repository;

import id.rest.model.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author mangprang
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    public Page<Customer> findAll(Specification<Customer> spec, Pageable pageable);
    public List<Customer> findAll(Specification<Customer> spec);
    public List<Customer> findAllByNameIn(List<String> names);

}
