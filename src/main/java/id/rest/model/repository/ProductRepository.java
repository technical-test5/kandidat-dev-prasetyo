/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rest.model.repository;

import id.rest.model.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author mangprang
 */
public interface ProductRepository extends JpaRepository<Product, Long> {

    public Page<Product> findAll(Specification<Product> spec, Pageable pageable);
    public List<Product> findAll(Specification<Product> spec);
    public List<Product> findAllByNameIn(List<String> names);

}
