/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rest.model.repository;

import id.rest.model.entity.SalesInvoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author mangprang
 */
public interface SalesInvoiceRepository extends JpaRepository<SalesInvoice, Long> {

    public Page<SalesInvoice> findAll(Specification<SalesInvoice> spec, Pageable pageable);
    public Page<SalesInvoice> findAll(Pageable pageable);
    public List<SalesInvoice> findAll(Specification<SalesInvoice> spec);

}
