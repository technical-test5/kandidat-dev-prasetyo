package id.rest.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="bale_sales_invoice_line")
@EqualsAndHashCode(callSuper = false)
public class SalesInvoiceLine extends AbstractAuditableEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="amount")
    private Double amount;
    @Column(name="discount_amount")
    private Double discountAmount;
    @Column(name="total_amount")
    private Double totalAmount;

    @ToString.Exclude
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "sales_invoice_id", referencedColumnName = "id")
    private SalesInvoice salesInvoice;

    @ToString.Exclude
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;


}
