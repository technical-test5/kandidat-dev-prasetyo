package id.rest.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="bale_sales_invoice")
@EqualsAndHashCode(callSuper = false)
public class SalesInvoice extends AbstractAuditableEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "inv_number", unique = true, nullable = false)
    private String invoiceNumber;
    @Column(name="invoice_date")
    private LocalDate invoiceDate;
    @Column(name="amount")
    private Double amount;
    @Column(name="total_discount_amount")
    private Double totalDiscountAmount;
    @Column(name="total_amount")
    private Double totalAmount;

    @ToString.Exclude
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    private Customer customer;

    @ToString.Exclude
    @JsonIgnore
    @OneToMany(mappedBy = "salesInvoice")
    private List<SalesInvoiceLine> salesInvoiceLines = new ArrayList<>();


}
