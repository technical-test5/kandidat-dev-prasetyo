package id.rest.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public class DateUtil {

    public static String localDateTimeToStringUTC(LocalDateTime dateTime) {
        if (dateTime == null) return "";

        ZoneId utc = ZoneId.systemDefault();
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(utc);
        return dateTime.format(formatter);
    }

    public static String localDateTimeToStringIDN(LocalDateTime dateTime) {
        if (dateTime == null) return "";

        ZoneId utc = ZoneId.systemDefault();
        ZoneId idn = TimeZone.getTimeZone("GMT+7:00").toZoneId();
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(idn);
        return dateTime.atZone(utc).format(formatter);
    }
}
