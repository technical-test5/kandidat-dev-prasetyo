/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rest.service;

import id.rest.model.dto.*;
import id.rest.model.entity.Customer;
import id.rest.model.entity.Product;
import id.rest.model.entity.SalesInvoice;
import id.rest.model.entity.SalesInvoiceLine;
import id.rest.model.mapper.CustomerMapper;
import id.rest.model.mapper.ProductMapper;
import id.rest.model.mapper.SalesInvoiceMapper;
import id.rest.model.repository.CustomerRepository;
import id.rest.model.repository.ProductRepository;
import id.rest.model.repository.SalesInvoiceLineRepository;
import id.rest.model.repository.SalesInvoiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author mangprang
 */

@Slf4j
@Service
@Transactional(readOnly = true)
public class SalesService {

	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private SalesInvoiceRepository salesInvoiceRepository;
	@Autowired
	private SalesInvoiceLineRepository salesInvoiceLineRepository;
	@Autowired
	private SalesInvoiceMapper invoiceMapper;
	@Autowired
	private ProductMapper productMapper;
	@Autowired
	private CustomerMapper customerMapper;


	@Transactional
	public Page<ProductDto> getProducts(Pageable pageable) {
		if (productRepository.count() == 0) {
			Product p1 = new Product();
			p1.setName("Samsung A1");
			p1.setDescription("Samsung Smartphone");
			p1.setAmount(3000000D);

			Product p2 = new Product();
			p2.setName("Samsung A5");
			p2.setDescription("Samsung Smartphone");
			p2.setAmount(5000000D);

			productRepository.save(p1);
			productRepository.save(p2);
		}

		return productRepository.findAll(pageable)
				.map(productMapper::toDto);
	}

	@Transactional
	public Page<CustomerDto> getCustomer(Pageable pageable) {
		if (customerRepository.count() == 0) {
			Customer p1 = new Customer();
			p1.setName("Jone Doe");
			p1.setEmail("jon@mail.com");

			Customer p2 = new Customer();
			p2.setName("Jone Smith");
			p2.setEmail("smith@mail.com");

			customerRepository.save(p1);
			customerRepository.save(p2);
		}

		return customerRepository.findAll(pageable)
				.map(customerMapper::toDto);
	}

	public Page<SalesInvoiceDto> getSalesInvoice(Pageable pageable) {
		return salesInvoiceRepository.findAll(pageable)
				.map(invoiceMapper::toDto);
	}

	public SalesInvoiceDetailDto getSalesInvoiceDetail(Long salesInvoiceId) {
		SalesInvoice invoice = salesInvoiceRepository.findById(salesInvoiceId).orElse(new SalesInvoice());
		return invoiceMapper.toDetailDto(invoice);
	}

	@Transactional
	public String submitSales(SalesPostDto dto) {
		try {
			Customer customer = customerRepository.findById(dto.getCustomerId()).orElse(null);
			SalesInvoice invoice = new SalesInvoice();
			invoice.setInvoiceDate(LocalDate.now());
			invoice.setInvoiceNumber(UUID.randomUUID().toString());
			invoice.setCustomer(customer);
			invoice.setAmount(0D);
			invoice.setTotalAmount(0D);
			invoice.setTotalDiscountAmount(0D);

			List<SalesInvoiceLine> invoiceLines = new ArrayList<>();
			dto.getItems().forEach(q -> {
				Product product = productRepository.findById(q.getProductId()).orElse(null);
				if (product != null) {
					double amount = q.getQty();
					double totalAmount = q.getQty() * product.getAmount();
					double discountAmount = 0;

					invoice.setTotalDiscountAmount(invoice.getTotalDiscountAmount() + discountAmount);
					invoice.setTotalAmount(invoice.getTotalAmount() + totalAmount);
					invoice.setAmount(invoice.getAmount() + amount);

					SalesInvoiceLine invoiceLine = new SalesInvoiceLine();
					invoiceLine.setSalesInvoice(invoice);
					invoiceLine.setAmount(amount);
					invoiceLine.setTotalAmount(totalAmount);
					invoiceLine.setDiscountAmount(discountAmount);
					invoiceLine.setProduct(product);
					invoiceLines.add(invoiceLine);
				}
			});

			salesInvoiceRepository.save(invoice);
			salesInvoiceLineRepository.saveAll(invoiceLines);
			return "Transaksi berhasil";
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

}

