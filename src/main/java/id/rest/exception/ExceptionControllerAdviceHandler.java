/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rest.exception;

import id.rest.util.ApiResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mangprang
 */
@RestControllerAdvice
public class ExceptionControllerAdviceHandler {

    @ExceptionHandler(RecordNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ApiResponseUtil handleNoRecordFoundException(RecordNotFoundException ex) {

        ApiResponseUtil errorResponse = new ApiResponseUtil();
        errorResponse.setCode(String.valueOf(HttpStatus.NOT_FOUND.value()));
        errorResponse.setMessage("No Record Found");
        return errorResponse;
    }

    @ExceptionHandler(IdNotMatchException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ResponseBody
    public ApiResponseUtil handleNoRecordFoundException(IdNotMatchException ex) {

        ApiResponseUtil errorResponse = new ApiResponseUtil();
        errorResponse.setCode(String.valueOf(HttpStatus.NOT_ACCEPTABLE.value()));
        errorResponse.setMessage("Unacceptable parameters");
        return errorResponse;
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiResponseUtil handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        ApiResponseUtil errorResponse = new ApiResponseUtil();
        errorResponse.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
        errorResponse.setMessage("Invalid parameters");
        errorResponse.setData(errors);
        return errorResponse;
    }

}
